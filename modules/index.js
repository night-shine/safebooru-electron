const { default: axios } = require('axios')
const xml = require('xml2json')

const urlApi = {
  baseUrl: 'https://safebooru.org',
  searchUrl: 'https://safebooru.org/index.php?page=dapi&s=post&q=index&json=1&limit=1',
  commentUrl: 'https://safebooru.org/index.php?page=dapi&s=comment&q=index&'
}

//auxiliar function to build the image url, the image tags and the images comments
const buildImage = async (props) => {

  let imgTags = props.tags.split(' ')
  imgTags.shift(); imgTags.pop()

  let commentFecth = await axios({
    method: 'get',
    url: `${urlApi.commentUrl}&post_id=${props.id}`,
    responseType: 'xml'
  })

  commentFecth = JSON.parse(xml.toJson(commentFecth.data))

  let commentImage = commentFecth.comments.comment.map((data) => {
    return {
      user: data.creator,
      body: data.body,
      date: data.created_at
    }
  })

  return {
    fileUrl: `${urlApi.baseUrl}/images/${props.directory}/${props.image}`,
    tags: imgTags,
    comments: commentImage
  }
}

// fetch the image by id and return the image with more propeties
const imageLoad = async (args) => {

  let fetch = await axios({
    method: 'get',
    url: `${urlApi.searchUrl}&id=${args.id}`
  })
  fetch = fetch.data

  return buildImage(fetch[0])
}


// auxiliar function that build the response to the client
const buildImageFeed = (props) => {
  return {
    feedImg: `${urlApi.baseUrl}/samples/${props.directory}/sample_${props.image}`,
    id: props.id
  }
}

// fetch the api with the search arg and return the feed
const feedLoad = async (args) => {

  let searchUrl = `${urlApi.searchUrl}&tags=${args.tags}`

  let fetch = await axios({
    method: 'get',
    url: searchUrl
  })

  fetch = fetch.data

  let fetchRes = fetch.map((data) => {
    return buildImageFeed(data)
  })

  return fetchRes
}

module.exports = { feedLoad, imageLoad }