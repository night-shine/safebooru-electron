import NavBAr from './components/NavBar';
import './App.css';

function App() {
  return (
    <div className="App">
      <NavBAr />
    </div>
  );
}

export default App;
