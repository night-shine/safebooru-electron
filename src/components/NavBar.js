
import { useState } from 'react'

import searchIcon from '../../src/icons/search-icon.svg'
import menuIcon from '../../src/icons/menu-icon.svg'

import './navBar-style.css'

const NavBAr = () => {
  const [searchArg, setSearchArg] = useState('')

  const addContent = (event) => {
    setSearchArg(event.target.value)
  }

  return (
    <div className='nav-bar'>
      <button className='menu-nav-bar'>
        <img src={menuIcon}/>
      </button>
      <input onChange={addContent} type='text' placeholder='Search' />
      <button>
      <img src={searchIcon} height='fit-content'/>
      </button>
    </div>
  )
}

export default NavBAr;